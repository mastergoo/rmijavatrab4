/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * 
 * @author aluno
 */
public class ServerSum implements Sum {

	private String host;

	public static void main(String[] args) {
		try {
			ServerSum server = new ServerSum();
			server.host = args[0];
			Sum stubsum = (Sum) UnicastRemoteObject.exportObject(server, 0);
			Registry reg = LocateRegistry.getRegistry(server.host);
			reg.rebind("//" + server.host + "/sum", stubsum);
			System.err.println("Servidor carregado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public long getSum(int n) throws RemoteException {
		return (n * (n + 1) / 2);
	}

}
