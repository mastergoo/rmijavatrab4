/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * 
 * @author aluno
 */
public class ServerRaizes implements Raizes {

	private String host;

	public static void main(String[] args) {
		try {
			ServerRaizes server = new ServerRaizes();
			server.host = args[0];
			Raizes stubx = (Raizes) UnicastRemoteObject.exportObject(server, 0);
			Registry reg = LocateRegistry.getRegistry(server.host);
			reg.rebind("//" + server.host + "/raizes", stubx);
			System.err.println("Servidor carregado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public double[] getRaizes(int a, int b, int c) throws RemoteException {
		double delta = b * b - 4 * (a * c);
		if (delta < 0 || a == 0) {
			return new double[] { 0, 0 };
		}
		double ans[] = new double[2];
		ans[0] = (double) (-b) + Math.sqrt(delta) / (double) (2d * a);
		ans[1] = (double) (b) + Math.sqrt(delta) / (double) (2d * a);
		return ans;
	}

}
