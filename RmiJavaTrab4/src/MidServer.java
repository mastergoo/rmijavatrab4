import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MidServer implements Fibonacci, Raizes, Sum, Mideable {

	private String hostLocal;
	private String hostSum = "192.168.195.130";
	private String hostRaizes = "192.168.195.130";
	private String hostFibo = "192.168.195.130";

	public static void main(String[] args) {
		try {
			MidServer server = new MidServer();
			server.hostLocal = args[0];
			Mideable stubmid = (Mideable) UnicastRemoteObject.exportObject(server, 0);
			Registry reg = LocateRegistry.getRegistry(server.hostLocal);
			reg.rebind("//" + server.hostLocal + "/serverreq", stubmid);
			System.err.println("Servidor carregado");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public long getSum(int n) throws RemoteException {
		try {
			Registry regSum = LocateRegistry.getRegistry(hostSum);
			Sum stubsum = (Sum) regSum.lookup("//" + hostSum + "/sum");
			return stubsum.getSum(n);
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public long getNesimo(int n) throws RemoteException {
		try {
			Registry regFibo = LocateRegistry.getRegistry(hostFibo);
			Fibonacci stubsum = (Fibonacci) regFibo.lookup("//" + hostFibo + "/fibo");
			return stubsum.getNesimo(n);
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public double[] getRaizes(int a, int b, int c) throws RemoteException {
		try {
			Registry regRaiz = LocateRegistry.getRegistry(hostRaizes);
			Raizes stubRaizes = (Raizes) regRaiz.lookup("//" + hostRaizes + "/raizes");
			return stubRaizes.getRaizes(a, b, c);
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
