import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Mideable extends Remote {

	public long getSum(int n) throws RemoteException;

	public long getNesimo(int n) throws RemoteException;

	public double[] getRaizes(int a, int b, int c) throws RemoteException;

}
