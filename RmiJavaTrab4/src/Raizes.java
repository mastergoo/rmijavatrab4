/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author aluno
 */
public interface Raizes extends Remote {

    public double[] getRaizes(int a, int b, int c) throws RemoteException;
}
