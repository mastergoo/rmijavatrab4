/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente {

	public static void main(String[] args) {
		String hostmid = args[0];
		try {
			Registry registrymid = LocateRegistry.getRegistry(hostmid);
			Mideable stubmid = (Mideable) registrymid.lookup("//" + hostmid + "/serverreq");
			System.out.println("Fibo n-esimo: " + stubmid.getNesimo(5));
			System.out.println("Sum: " + stubmid.getSum(5));
			double[] raizes = stubmid.getRaizes(2, 7, -3);
			System.out.println("Raizes, x1: " + raizes[0] + ", x2: " + raizes[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
